<!-DB에 테이블 생성하는 jsp파일->

<!-hmtl에서 한글 설정, 브라우저에게 encoding형식 알림->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-jsp에서 한글 설정->
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<h1> 테이블만들기 OK </h1>
</head>
<body>
<%
	//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
	//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  //sql쿼리 생성/실행을 위한 statement객체 생성
	  //connection객체의 createStatement() 메소드를 시용한다
	  Statement stmt = conn.createStatement();
	try{  
	  //테이블 생성 쿼리
	  stmt.execute("create table examtable("+
		"name varchar(20),"+
		"studentid int not null primary key,"+
		"kor int,"+
		"eng int,"+
		"mat int)"+
		"DEFAULT CHARSET=utf8;");
	  //전체 테이블 출력
	  stmt.execute("show tables;");
	  ResultSet rset = stmt.executeQuery("show tables;");
	  //stmt.executeQuery: recordSet 반환, select문에서 사용, 결과로 ResultSet반환
	  //stmt.executeUpdate:성공한 row수 반환, insert, update, delete문에서 사용
	 
	 //ResultSet처리: resultSet으로 부터 원하는 데이터 추출하는 과정
	 //rset.next()로 한 행씩 처리, rset의 첫 번째 필드는 1 부터 시작
	 while(rset.next()){
		 out.println(rset.getString(1)+"<br>");
	 }
	 rset.close(); 
	 stmt.close();
	 conn.close();
	}
	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("이미 추가된 테이블입니다.");
         }
   }
%>
</body>
</html>	