<!-DB 내 테이블에 데이터 입력하는 jsp 파일->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<h1> 실습데이터 입력 </h1>
</head>
<body>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  Statement stmt = conn.createStatement();
	  try{
	  //데이터 입력 sql 명령문 실행
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('나연', 209901, 95, 100, 95);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('정연', 209902, 100, 100, 100);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('모모', 209903, 100, 90, 100);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('사나', 209904, 100, 95, 90);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('지효', 209905, 80, 100, 70);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('미나', 209906, 95, 90, 95);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('다현', 209907, 100, 90, 100);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('채영', 209908, 100, 75, 90);");
	  stmt.execute("insert into examtable(name, studentid, kor, eng, mat) values('쯔위', 209909, 100, 100, 70);");
	  
	 stmt.close();
	 conn.close();
	  }
	  	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("이미 삭제된 테이블입니다.");
         }
   }
%>
</body>
</html>	