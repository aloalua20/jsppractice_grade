<!-- DB에서 테이블 삭제하는 jsp 파일-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<h1> 테이블지우기 OK </h1>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  Statement stmt = conn.createStatement();
	  Statement stmt2 = conn.createStatement();
	  try{
	 stmt.execute("drop table examtable;");
	 stmt.execute("show tables;");
	  ResultSet rset = stmt.executeQuery("show tables;");
	 while(rset.next()){
		 out.println(rset.getString(1)+"<br>");
	 }
	 rset.close();
	 stmt.close();
	 conn.close();
	 }
	 	 	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("이미 삭제된 테이블입니다.");
         }
   }
%>
</table>
</body>
</html>	