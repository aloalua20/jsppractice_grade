<!- 데이터 입력하는 파일 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<!-한글 get/post 파라메터를 위해 네트워크 관련 클래스를 가지는 java.net 패키지 임포트->
<%@ page import="java.net.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>

<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  Statement stmt = conn.createStatement();
	  Statement stmt2 = conn.createStatement();

	try{
	 int firstNum=209901;
	 int addStudentID=0;
	 addStudentID=firstNum;
	 ResultSet rset2 = stmt2.executeQuery("select studentid from examtable;");
	 while(rset2.next()){
		 if(addStudentID==rset2.getInt(1)){
			addStudentID=addStudentID+1;
		 }	else{
			 break;
		 }		
	 }

	 //inputForm에서 post 방식으로 얻은 파라메터 name을 담는 name 문자열
	 request.setCharacterEncoding("UTF-8");
	 String name = request.getParameter("name");
	
	 
	stmt.execute("insert into examtable (name,studentid,kor,eng,mat) " 
				+"values('"+request.getParameter("name")+"',"
				+Integer.toString(addStudentID)+","
				+request.getParameter("kor")+","
				+request.getParameter("eng")+","
				+request.getParameter("mat")+");");	
	 	  
	rset2.close();
	stmt.close();
	stmt2.close();
	conn.close();
	
%>

<h1> 성적입력추가완료 </h1>
</head>
<body>

<br>
	<table cellspacing=1 width=300 border=1 style="font-size:20;">
	<tr>
		<td width=150><p align=center> 이름 </p></td>
		<td width=150><p align=center>
		 <!-input tag: form요소 안에서 사용, 입력영역 지정, readonly로 사용자가 수정 불가
			입력창 안에 name 으로 이름 조회됨->
		 <input style = "text-align:center;" type=text name='name' value=<%=request.getParameter("name")%> readonly maxlength=20 required 
	pattern='^[가-힣a-zA-Z]*$' onclick="check(); "></p>
		</td>
	</tr>
	<tr>
		<td width=150><p align=center> 학번 </p></td>
		<td width=150><p align=center>
		 <!-입력창 안에 학번 조회 됨->
		 <input type=number min="0" max="100" name="studentid" value=<%=Integer.toString(addStudentID)%> readonly ></input></p>
		</td>
	</tr>
	<tr>
		<td width=150><p align=center> 국어 </p></td>
		<td width=150><p align=center>
		 <!-입력창 안에 국어성적 조회 됨->
		 <input style = "text-align:center;" type=number name='kor' required value=<%=request.getParameter("kor")%> readonly ></input></p>
		</td>
	</tr>
	<tr>
		<td width=150><p align=center> 영어 </p></td>
		<td width=150><p align=center>
		 <!-입력창 안에 영어성적 조회 됨->
		 <input style = "text-align:center;" type=number  name='eng' required value=<%=request.getParameter("eng")%> readonly ></input></p>
		</td>
	</tr>
	<tr>
		<td width=150><p align=center> 수학 </p></td>
		<td width=150><p align=center>
		 <!-입력창 안에 수학성적 조회 됨->
		 <input style = "text-align:center;" type=number name='mat' required value=<%=request.getParameter("mat")%> readonly ></input></p>
		</td>
	</tr>
</table>
<br>
<!-"뒤로가기"버튼 이용 시 inputForm1.html 파일로 이동->
<form method='post' action='inputForm1.html'>
	<table cellspacing=1 width=300 border=0>
	<tr></tr>
	<tr>
		<TD width=200>
		<td width=100 ><p align=right><input type="submit" value="뒤로가기"></td>
	</tr>
	</table>
</form>
<%
	}
	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("후보자 이름이 깁니다.");
         }
	}
	 
%>
</body>
</html>
