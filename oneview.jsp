<!-하나의 데이터만 조회하는 파일->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
</head>
<body>

<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  Statement stmt = conn.createStatement();
	  //get으로 받아 string이므로 String 변수명1 = request.getParameter(기존변수명); 으로 값 받아옴
	  //이전 페이지에서 key로 지정 된 파라메터를 받아 key문자열에 적용
	  String key = request.getParameter("id");
	  
	 //1번 필드(학생이름)이 일치하는 데이터 조회
	 ResultSet rset = stmt.executeQuery("select * from examtable where studentid ='"+key+"';");
%>
<h1> [<%=key%>] 조회</h1>
<table cellspacing=1 width=600 border=1>
	<tr>
		<td width=50><p align=center>이름</p></td>
		<td width=50><p align=center>학번</p></td>
		<td width=50><p align=center>국어</p></td>
		<td width=50><p align=center>영어</p></td>
		<td width=50><p align=center>수학</p></td>
	</tr>
<%
	while (rset.next()) {
		out.println("<tr>");
		out.println("<td width=50><p align=center>"+rset.getString(1)+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(2))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(3))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(4))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(5))+"</p></td>");
		out.println("<tr>");
		}
	 rset.close();
	 stmt.close();
	 conn.close();
%>
</table>
</body>
</html>	