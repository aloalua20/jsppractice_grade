<!- 데이터 삭제하는 파일 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="java.sql.*, javax.sql.*, java.io.*, java.net.*" %>
<html>
<head>
<style type = "text/css">

</style>
</head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<body>
<h1>레코드 삭제</h1>

<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase","root","aeae");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
try{
	String stdid = request.getParameter("key");
	String sql = "delete from examtable where studentid = "+stdid+"";
	stmt.executeUpdate(sql);
	
	ResultSet rset = stmt2.executeQuery("select name, studentid, kor, eng, mat, (kor+eng+mat) as 총점,"+
	"(kor+eng+mat)/3 as 평균,(select count(*)+1 from examtable where (kor+eng+mat) > (b.kor+b.eng+b.mat)) as 등수 "+
	"from examtable b;");


%>

<h2><%=stdid%> 학생이 삭제되었습니다.</h2>
<table cellspacing=1 width=600 height = 20 border=1 style="font-size:20;">
	<tr bgcolor='E0F8F7'>
		<td width=120><p align=center>이름</p></td>
		<td width=120><p align=center>학번</p></td>
		<td width=120><p align=center>국어</p></td>
		<td width=120><p align=center>영어</p></td>
		<td width=120><p align=center>수학</p></td>
		<td width=120><p align=center>총점</p></td>
		<td width=120><p align=center>평균</p></td>
		<td width=120><p align=center>등수</p></td>
	</tr>
<%
	while (rset.next()) {

		out.println("<tr>");

		out.println("<td width=120><p align=center>"+rset.getString(1)+"</a></p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(2))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(3))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(4))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(5))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(6))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(7))+"</p></td>");
		out.println("<td width=50><p align=center>"+Integer.toString(rset.getInt(8))+"</p></td>");
		out.println("</tr>");

		}
	 rset.close();
	 stmt.close();
	 stmt2.close();
	 conn.close();
	 }
	 	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("이미 삭제된 데이터입니다.");
         }
   }
%>
</table>
</body>
</html>
	