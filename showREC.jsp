<!- inputForm2.html에서 받아온 학번 파라메터로 검색하는 파일 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="java.sql.*, javax.sql.*, java.io.*, java.net.*" %>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
</head>
<body>

<SCRIPT LANGUAGE="JavaScript">
<!-숫자 입력만 받는 메소드->
function validate() 
{
    var stdid= document.getElementById("studentid");
	var onlyNum = /[^0123456789]
	if(stdid.value=="") {
		alert("학생번호를 입력해주세요")
		stdid.focus();
		return false;
	}
	if(!check(stdid, onlyNum "잘못된 입력입니다.")){
		return false;
	}
}
</SCRIPT>

<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase","root","aeae");
	Statement stmt = conn.createStatement();
	try{
	 String id = request.getParameter("id");
	 
	 if(id.length() ==0) {
		 id=""; //editing
		 String alert ="해당학번없음";
		 out.println(alert);
	 }
	ResultSet rset = stmt.executeQuery("select * from examtable where studentid = "+id);
	String name="해당학생없음", studentid="", kor="", eng="", mat="";
	while(rset.next()) {
		 name=rset.getString(1);
		 studentid=Integer.toString(rset.getInt(2));
		 kor=Integer.toString(rset.getInt(3));
		 eng=Integer.toString(rset.getInt(4));
		 mat=Integer.toString(rset.getInt(5));
		
	}
	
	
	
	
	rset.close();
	stmt.close();
	conn.close();
	
%>

<h1> 성적 조회 후 정정 / 삭제 </h1>
<br>
<form method='post' action='updateDB.jsp'>
	<table width=300 border="1" cellspacing=0 cellpadding=5>
	<tr>
		<td width=150><p align=center>이름</p></td>
		<td width=150><p align=center><input type='text' name='name' value='<%=name%>'></p></td>
	</tr>
	<tr>
		<td width=150><p align=center>학번</p></td>
		<td width=150><p align=center><input type='text' name='studentid' value='<%=studentid%>' readonly ></p></td>
	</tr>
	<tr>
		<td width=150><p align=center>국어</p></td>
		<td width=150><p align=center><input type='text' name='kor' value='<%=kor%>'></p></td>
	</tr>
	<tr>
		<td width=150><p align=center>영어</p></td>
		<td width=150><p align=center><input type='text' name='eng' value='<%=eng%>'></p></td>
	</tr>
	<tr>
		<td width=150><p align=center>수학</p></td>
		<td width=150><p align=center><input type='text' name='mat' value='<%=mat%>'></p></td>
	</tr>
	</table>

	<br>

 <% 
	if(studentid.length()!= 0) {
%>
		<table align=center cellspacing=1 width=200 height = 20  style="font-size:20;">
		<tr>
		<td width=100><p align=right><input type=submit value="수정" ></p></td>
		<td width=100><p align=right><input type=submit formaction='deleteDB.jsp?key=<%=studentid%>' value="삭제"></p></td>
		</tr>
		</table>
<%
	}
	}
		catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("없는 학번입니다.");
         }
   }
 %>
</form>
</body>
</html>
