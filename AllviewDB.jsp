<!-전체 데이터 조회하는 jsp 파일->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>

<html>
<head>
<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h1 {
   text-align:center;
}   
</style>
<h1 style=align:center> 전체조회 </h1>
</head>
<body>
<%
    String start_point_datas = request.getParameter("want_page"); // 페이지 변경시 시작 번호를 변경하기 위한 변수 계산  
	int start_point_data;                                            
try{
	// getParameter는 문자열로 인자를 받기 때문에 int형으로 형변환이 필요하다
     start_point_data = Integer.parseInt(start_point_datas);        
} catch (Exception e){
	//try catch 구문을 활용하여 초기 변수값이 없어 발생하는 오류를 응용, fromPT 에 초기 값을 부여하였다.
     start_point_data = 0;      //현재페이지                    
  }

if(start_point_data <0){
       start_point_data = 0;
}       
   
    Class.forName("com.mysql.jdbc.Driver");
   Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase","root","aeae");
     Statement stmt = conn.createStatement();
    Statement stmt2 = conn.createStatement();
   
    ResultSet rset2 = stmt2.executeQuery("select count(*) from examtable;");
    
    ResultSet rset = stmt.executeQuery("select name, studentid, kor, eng, mat, (kor+eng+mat) as 총점,"+
	"(kor+eng+mat)/3 as 평균,(select count(*)+1 from examtable where (kor+eng+mat) > (b.kor+b.eng+b.mat)) as 등수 "+
	"from examtable b;");

	int totaldata = 0;  // 전체 데이터 수
    int countpage = 5;    //한화면에 나올 페이지번호 수 
    int datalist = 5;   //출력될 데이터 수
    int totalpage = 0;     //전체 페이지 수
    int linecount = 0;
	 
	rset2.next();
   totaldata = rset2.getInt(1); // 전체 데이처 수 계산

       
   if(totaldata != 0){
      if((totaldata % datalist) == 0){
         totalpage = (totaldata/datalist);
      }else{
         totalpage = (totaldata/datalist) + 1;
      } 
   }      
   //총페이지 수 계산
   if(start_point_data >= (totalpage*datalist)-datalist){
      start_point_data = (totalpage*datalist)-datalist;
    }   // 맨마지막 페이지를 클릭 했을때 다음 페이지로 넘어가못하도록 출력되는 페이지의 한계숫자를 조정
%>
<table cellspacing=1 width=600 height = 20 border=1 style="font-size:20;">
<tr>
<td width=120><p align=center>이름</td>
<td width=120><p align=center>학번</td>
<td width=120><p align=center>국어</td>
<td width=120><p align=center>영어</td>
<td width=120><p align=center>수학</td>
<td width=120><p align=center>총점</td>
<td width=120><p align=center>평균</td>
<td width=120><p align=center>순위</td>
</tr>
<%     
    while (rset.next()){  
	// ~ 번부터 ~번 까지 출력
      if (linecount < start_point_data){         
          linecount++; 
          continue;
      }
%>           
        <tr>
        <td width=120><p align=center><a href='oneview.jsp?id=<%=rset.getString(2)%>'>
		<%=rset.getString(1)%></a></p></td>
        <td width=120><p align=center><%=Integer.toString(rset.getInt(2))%></p></td>  
        <td width=120><p align=center><%=Integer.toString(rset.getInt(3))%></p></td>
        <td width=120><p align=center><%=Integer.toString(rset.getInt(4))%></p></td>
        <td width=120><p align=center><%=Integer.toString(rset.getInt(5))%></p></td> 
        <td width=120><p align=center><%=Integer.toString(rset.getInt(6))%></p></td> 
        <td width=120><p align=center><%=rset.getDouble(7)%></p></td>
        <td width=120><p align=center><%=Integer.toString(rset.getInt(8))%></p></td>         
        </tr>
<%		   
        linecount++;
		
		if(linecount >= start_point_data+datalist){
             break;
      }
                  
    }     
%>
</table>
<%
	String first = "처음";
    String before = "이전";    
    String nex = "다음";
    String over = "끝";
	//클릭한 번호에 따라 보이는 페이지 번호의 시작과 끝을 변경하는 식
     int startpage = (start_point_data/(countpage*datalist)) * countpage +1;  
     int endpage = startpage + countpage -1;
    // 맨마지막 페이지의 번호만 나오도록 endpage숫자가 총 페이지 수보다 클 경우 강제로 총페이지수를 입력
    if(endpage > totalpage) {
       endpage = totalpage;
    }                               
%>
<br><br>
<table cellspacing=1 width=300 style="font-size:20;">
<tr>
<td>
<%    
    //페이지 번호 출력 식  
    if(startpage >=1){
       out.println("<a href = AllviewDB.jsp?want_page="+ 0 +">"+" "+ first +" "+"</a></td>");
    }
    
    if(start_point_data>=0){
       out.println("<td><a href=AllviewDB.jsp?want_page="+ (start_point_data -datalist) +">"+" "+ before +" "+"</a></td>");
    }

     for(int i = startpage; i <= endpage; i++){
       if (i == start_point_data){          
         out.println("<td><a href = AllviewDB.jsp?want_page="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>");
       }        
       else {
       out.println("<td><a href = AllviewDB.jsp?want_page="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>"); 
      
    }    //출력될 페이지 번호 계산
    }
    
    if (start_point_data < totalpage*datalist) {    
    out.println("<td><a href=AllviewDB.jsp?want_page="+ (start_point_data + datalist) +">"+" "+ nex +" "+"</a></td>");
    }
	
    if (endpage <= totalpage) {
    out.print("<td><a href=AllviewDB.jsp?want_page="+ endpage*datalist +">"+" "+ over +" "+"</a></td>"); 
   // 페이지 이동의 편리성을 위해 endpage를 사용하여 한번누르면 맨 마지막 번호군의 앞번호로 이동
   //원 클릭에 제일 마지막번호로 바로 이동하기 위해서는 totalpage변수를 사용
      rset.close();
      stmt.close();
      conn.close();
	}      
    
%>    
 </tr>
</td>
</table> 

</body>
</html>     